# Acessórios para operadores Hádron
LHADRON

Este pacote tem função de fazer a atualização do sistema Hádron, um dos softwares desenvolvidos pela empresa Prócion sistemas
para efetuar a atualização é necessário que o terminal já esteja instalado, e que o mapeamento do dispositivo esteja feito na máquina.

Antes de a atualização ser iniciada é necessário que se especifique qual versão será utilizada, e em qual estação serão armazenados os drivers do programa.
A atualização usará o servidor FTP para  buscar os arquivos necessários, fará a extração dos arquivos RAR e após confirmação do operador, fara a troca entre os Drivers atuais e o novos.

Requisitos:
É nessessário que o descompactador de dados WinRar esteja instalado

Este programa usa a pasta padrão %programfiles%/WinRar para acessar o descompactador, caso a maquina seja 86x é indicado modificar o mapeamento. 


OBS:
O login e senha de acesso às transferencias pelo FTP devem ser editadas no código
----------------------------------------------------------------------------------------------------------------------------------
BACKUP - onedrive

Este pacote tem intuito de facilitar e garantir a segurança dos dados dos clientes, fazendo de forma dinamica, a compactação da pasta de arquivos base e enviando para o serviço de armazenamento em nuvem.

Para uma primeira utilização é necessário que se configure para o uso posterior do cliente.
assim que aberto o programa, deve-se especificar os parametros necessários:

A sigla do cliente (apenas 3 caracteres)
A letra de mapeamento do Drive de armazenamento (deve inserir apenas a letra)
O nome da pasta de arquivos base.

feito isso o programa, criará um arquivo com as configurações a serem seguidas.

Requisitos:
Diferente da Atualização, o Backup utiliza de outro compactador,winzip, a qual deve estar instalado na máquina para ser utilizado, o instalador pode ser encontrado no drive de rede (padrão P:), no diretório 'Backup'.
